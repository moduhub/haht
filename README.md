#Description

The Hash Map File System (HMFS) project aims to provide a driver to store data in a non-volatile medium (such as an SD card or NAND Flash IC) in a way that can be retrived through its hash value.

#Compatibility

This Lib is designed to work with Zephyr RTOS