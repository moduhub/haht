#ifndef _HMFS_H_
#define _HMFS_H_

#include "utils/BasicTypes.h"

#include "core/NonVolatileData.h"


#define HMFS_HASH_LENGTH 32
#define HMFS_BLOCK_SIZE 512

#define HMFS_FREE_BLOCK_MAGIC       0x78563412
#define HMFS_DATA_BLOCK_MAGIC       0xDEFE
#define HMFS_NODE_BLOCK_MAGIC       0xC0C0

#define HMFS_DESCRIPTOR_STATE_FREE	0
#define HMFS_DESCRIPTOR_STATE_USED	1

#define HMFS_MAX_BTREE_DEGREE       5 // 12 seria o ideal => bytes (8 + (40 * 12)) = 488

#define HMFS_MAX_DATA_PER_BLOCK     500

#define HMFS_MAX_BTREE_DEPTH        10

#define HMFS_DISK_SIZE              512000
//#define HMFS_DISK_NUM_BLOCKS        2097152

struct HMFSPointerNVData {

    uint32_t firstFreeBlock;
    uint32_t lastFreeBlock;
    uint32_t numFreeBlocks;
    
};


struct ResourceDataBlock {

    uint16_t	magic;
    uint16_t    dataSize;
    uint32_t    nextDataBlock;
    
    uint8_t     data[HMFS_MAX_DATA_PER_BLOCK];
    
    uint32_t	crc;

};


struct ResourceNodeBlock {
	
	uint16_t    magic;
	
    uint16_t    numKeys;
    
    uint8_t		resourceHash[HMFS_MAX_BTREE_DEGREE-1][HMFS_HASH_LENGTH];
    uint32_t    resourceBlockAddress[HMFS_MAX_BTREE_DEGREE-1];
    uint32_t    childNodeAddress[HMFS_MAX_BTREE_DEGREE];
	
	uint32_t	crc;

};


struct FreeDataBlock {
	
	uint32_t magic;
	uint32_t size;
    uint32_t prev;
	uint32_t next;
	uint32_t crc;
	
};

void printChar(uint8_t pChar);
void printHex(uint8_t* pData, int pSize);

void printTree();
void printNodeBlock(uint32_t pNodeAddress);

uint32_t HMFS_getNumFreeBlocks(void);

BasicReturnCode HMFS_writeBlock(uint32_t pBlockAddress, uint8_t *pData);

BasicReturnCode HMFS_readBlock(uint32_t pBlockAddress, uint8_t *pData);

BasicReturnCode HMFS_format(uint32_t pDiskSize, struct NVDataManager *pNVDataManager);

BasicReturnCode HMFS_initialize();
BasicReturnCode HMFS_initialize2(struct NVDataManager *pNVDataManager);

BasicReturnCode HMFS_initializeNVDataManager(   struct NVDataManager *pNVDataManager,
                                                uint8_t pIndex);

BasicReturnCode HMFS_checkFreeBlock(struct FreeDataBlock *pFreeBlock);

BasicReturnCode HMFS_checkDataBlock(struct ResourceDataBlock *pDataBlock);

BasicReturnCode HMFS_checkNodeBlock(struct ResourceNodeBlock *pNodeBlock);

BasicReturnCode HMFS_newBlock(uint32_t* tFreeBlockAddress);

BasicReturnCode HMFS_getResource(uint8_t *pHash, uint8_t *pData, uint32_t *pDataSize);

uint32_t HMFS_searchResourceAddress(uint8_t *pHash);

BasicReturnCode HMFS_storeResource(uint8_t *pData, uint32_t pDataSize, uint8_t *pHash);

BasicReturnCode HMFS_insertNodeBlock(uint8_t *pHash, uint32_t pBlockAddress);

BasicReturnCode HMFS_searchKeyInNode(struct ResourceNodeBlock *pNodeBlock, uint8_t *pHash, uint32_t *pKeyBlockAddress);

uint8_t HMFS_hashEquals(uint8_t *pHashA, uint8_t *pHashB);

BasicReturnCode HMFS_searchChildInNode(struct ResourceNodeBlock *pNodeBlock, uint8_t *pHash, uint32_t *pChildBlockAddress);

uint8_t HMFS_hashIsLesser(uint8_t *pHashA, uint8_t *pHashB);

uint8_t HMFS_hashIsGreater(uint8_t *pHashA, uint8_t *pHashB);

BasicReturnCode HMFS_insertKeyInNode(   struct ResourceNodeBlock *pNodeBlock,
                                        uint32_t pNodeAddress,
                                        uint8_t *pKey,
                                        uint32_t pKeyAddress,
                                        uint32_t pRightChild);

BasicReturnCode HMFS_splitRoot(struct ResourceNodeBlock *pNodeBlock);

BasicReturnCode HMFS_splitNode(uint32_t pParentNodeAddress, uint32_t pChildNodeAddress, struct ResourceNodeBlock *pChildNodeBlock, uint8_t* pParentIsFull);

#endif