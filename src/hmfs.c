
#include "file/hmfs.h"
#include "file/hmfs_diskio.h"

#include "crypto/SHA256.h"

#include <string.h>

uint32_t	gHMFS_firstFreeBlockAddress = 0;
uint32_t    gHMFS_lastFreeBlockAddress = 0;
uint32_t    gHMFS_numFreeBlocks = 0;

struct NVDataManager * gHMFS_pointersNVDataManager = 0;

uint8_t gHMFS_blockDataBuffer[HMFS_BLOCK_SIZE];
uint8_t gHMFS_splitBlock_blockDataBuffer[HMFS_BLOCK_SIZE];
uint8_t gHMFS_newBlock_blockDataBuffer[HMFS_BLOCK_SIZE];


uint32_t HMFS_getNumFreeBlocks(void) {
    
    return gHMFS_numFreeBlocks;
    
}

BasicReturnCode HMFS_format(uint32_t pDiskSize, struct NVDataManager *pNVDataManager) {

    BasicReturnCode tReturnCode = RETURN_SUCCESS;
    BasicReturnCode tResult;

    if(HMFS_DIO_initialize() != RETURN_SUCCESS) {
        return RETURN_ERROR;
    }
    
    uint32_t tCurrentBlockAddress = 1;
    uint32_t tNextBlockAddress;
    uint32_t tPrevBlockAddress;
    uint32_t tCalculatedCRC;
    
    uint32_t tNumBlocks = pDiskSize / HMFS_BLOCK_SIZE;
    
    struct FreeDataBlock *tFreeBlockHeader = (struct FreeDataBlock *)(gHMFS_blockDataBuffer);
    
    //Create linked chain of free blocks from block 1 to end
    tPrevBlockAddress = 0;

    do {

        memset(gHMFS_blockDataBuffer, 0x00, HMFS_BLOCK_SIZE);
        
        tNextBlockAddress = tCurrentBlockAddress + 1;

        if (tNextBlockAddress >= tNumBlocks) {
            tNextBlockAddress = 0;
        }

        tFreeBlockHeader->magic = HMFS_FREE_BLOCK_MAGIC;
        tFreeBlockHeader->size = HMFS_BLOCK_SIZE;
        tFreeBlockHeader->prev = tPrevBlockAddress;
        tFreeBlockHeader->next = tNextBlockAddress;
        tFreeBlockHeader->crc = 0;
        
        tCalculatedCRC = CRC_calculateCRC32((uint8_t*)(tFreeBlockHeader), sizeof(struct FreeDataBlock));
        tFreeBlockHeader->crc = tCalculatedCRC;

        tResult = HMFS_DIO_writeBlock(tCurrentBlockAddress, gHMFS_blockDataBuffer);
        
        if(tResult == RETURN_SUCCESS) {
            
            tResult = HMFS_DIO_readBlock(tCurrentBlockAddress, gHMFS_blockDataBuffer);
            
        } else {
            
            tReturnCode = RETURN_ERROR;
            break;
            
        }
        
        
        tPrevBlockAddress = tCurrentBlockAddress;
        tCurrentBlockAddress += 1;

    } while(tNextBlockAddress != 0);
    
    if(tReturnCode == RETURN_SUCCESS) {
    
        //Write the tree root at block 0
        struct ResourceNodeBlock *tRootBlock = (struct ResourceNodeBlock *)(gHMFS_blockDataBuffer);
        memset(gHMFS_blockDataBuffer, 0x00, HMFS_BLOCK_SIZE);

        tRootBlock->magic = HMFS_NODE_BLOCK_MAGIC;
        tRootBlock->numKeys = 0;
        tRootBlock->crc = 0;

        tCalculatedCRC = CRC_calculateCRC32((uint8_t *)(tRootBlock), sizeof(struct ResourceNodeBlock));
        tRootBlock->crc = tCalculatedCRC;

        tReturnCode = HMFS_DIO_writeBlock(0, gHMFS_blockDataBuffer);
        
        //Write pointers to NVData
        struct HMFSPointerNVData tNVData;
        
        tNVData.firstFreeBlock = 1;
        tNVData.lastFreeBlock = tNumBlocks-1;
        tNVData.numFreeBlocks = tNumBlocks-1;
        
        tResult = pNVDataManager->save(pNVDataManager, &tNVData, sizeof(struct HMFSPointerNVData));
        
        if(tResult != RETURN_SUCCESS) {
            tReturnCode = tResult;
        }
        
    }

    return tReturnCode;
    
}


BasicReturnCode HMFS_initialize2(struct NVDataManager *pNVDataManager) {
    
    BasicReturnCode tResult;
    BasicReturnCode tReturnCode = RETURN_SUCCESS;

    if(HMFS_DIO_initialize() != RETURN_SUCCESS) {
        return RETURN_ERROR;
    }
    
    //Fetch pointers from NVData
    gHMFS_firstFreeBlockAddress = 0;
    gHMFS_lastFreeBlockAddress = 0;
    gHMFS_numFreeBlocks = 0;
    
    gHMFS_pointersNVDataManager = pNVDataManager;
    
    struct HMFSPointerNVData tNVData;
    
    tResult = pNVDataManager->load(pNVDataManager, &tNVData, sizeof(struct HMFSPointerNVData));
    
    if(tResult == RETURN_SUCCESS) {
    
        // Check if first and last free are consistent
        
        struct FreeDataBlock *tFreeBlock = (struct FreeDataBlock *)(gHMFS_blockDataBuffer);

        tResult = HMFS_DIO_readBlock(tNVData.firstFreeBlock, gHMFS_blockDataBuffer);

        if (tResult == RETURN_SUCCESS) {

            //Check for valid free block
            tResult = HMFS_checkFreeBlock(tFreeBlock);
            
            if(tResult != RETURN_SUCCESS
            || tFreeBlock->prev != 0) {
                
                tReturnCode = RETURN_ERROR_CORRUTED_BLOCK;
                
            } else {

                tResult = HMFS_DIO_readBlock(tNVData.lastFreeBlock, gHMFS_blockDataBuffer);
                
                if (tResult == RETURN_SUCCESS) {

                    //Check for valid free block
                    tResult = HMFS_checkFreeBlock(tFreeBlock);

                    if(tResult != RETURN_SUCCESS
                    || tFreeBlock->next != 0) {
                        
                        tReturnCode = RETURN_ERROR_CORRUTED_BLOCK;
                        
                    } else {
                        
                        gHMFS_firstFreeBlockAddress = tNVData.firstFreeBlock;
                        gHMFS_lastFreeBlockAddress = tNVData.lastFreeBlock;
                        gHMFS_numFreeBlocks = tNVData.numFreeBlocks;
                        
                    }

                } else {
                    
                    tReturnCode = RETURN_ERROR_READ_FAILED;
                    
                }
                
            }
            
        } else {
            
            tReturnCode = RETURN_ERROR_READ_FAILED;
            
        }
        

        
    } else {
        
        tReturnCode = RETURN_ERROR;
        
    }
    
    return tReturnCode;
}


BasicReturnCode HMFS_initialize() {

    BasicReturnCode tResult;
    BasicReturnCode tReturnCode = RETURN_SUCCESS;
    
    uint32_t tCurrentBlockAddress;    
    uint8_t tAttempts = 0;

    if(HMFS_DIO_initialize() != RETURN_SUCCESS) {
        return RETURN_ERROR;
    }
    
    struct FreeDataBlock *tFreeBlock = (struct FreeDataBlock *)(gHMFS_blockDataBuffer);

    gHMFS_firstFreeBlockAddress = 0;
    gHMFS_lastFreeBlockAddress = 0;
    gHMFS_numFreeBlocks = 0;
    
    //Read entire disk, checking for first and last free block and all other blocks consistency
    
    tCurrentBlockAddress = 0;
    
    while(tCurrentBlockAddress < (HMFS_DISK_SIZE / HMFS_BLOCK_SIZE)) {

        tResult = HMFS_DIO_readBlock(tCurrentBlockAddress, gHMFS_blockDataBuffer);
        
        if (tResult == RETURN_SUCCESS) {

            //Check for valid free block
            tResult = HMFS_checkFreeBlock(tFreeBlock);

            if (tResult == RETURN_SUCCESS) {
                
                gHMFS_numFreeBlocks++;

                //Prev=0 means this is the first free block
                if (tFreeBlock->prev == 0) {
                    
                    if (gHMFS_firstFreeBlockAddress == 0) {
                    
                        gHMFS_firstFreeBlockAddress = tCurrentBlockAddress;

                    } else {
                        
                        // Invalid situation, two "firsts"
                        tReturnCode = RETURN_ERROR_CORRUTED_BLOCK;
                        break;
                        
                    }
                    
                }

                //Next=0 means this is the last free block
                if (tFreeBlock->next == 0) {
                
                    if (gHMFS_lastFreeBlockAddress == 0) {

                        gHMFS_lastFreeBlockAddress = tCurrentBlockAddress;

                    } else {
                        
                        // Invalid situation, two "lasts"
                        tReturnCode = RETURN_ERROR_CORRUTED_BLOCK;
                        break;

                    }
                    
                }

            } else {
            
                tResult = HMFS_checkNodeBlock(gHMFS_blockDataBuffer);

                if (tResult != RETURN_SUCCESS) {
            
                    tResult = HMFS_checkDataBlock(gHMFS_blockDataBuffer);
                            
                    if(tResult != RETURN_SUCCESS) {

                        //failed to read block, give up after 5 attempts
                        if(++tAttempts == 5) {
                            tReturnCode = RETURN_ERROR_CORRUTED_BLOCK;
                            break;
                        } else {
                            continue;
                        }
                        
                    }

                }
                
            }
            
        } else {
            
            //failed to read block, give up after 5 attempts
            if(++tAttempts == 5) {
                tReturnCode = RETURN_ERROR_READ_FAILED;
                break;
            } else {
                continue;
            }
            
        }
        
        tAttempts = 0;
        tCurrentBlockAddress++;
    }
    
    if(tReturnCode == RETURN_SUCCESS) {

        // TODO: Validate the Free block chain. (currently validating only first and last)        
        if ((gHMFS_firstFreeBlockAddress == 0) || (gHMFS_lastFreeBlockAddress == 0)) {
        
            //Did not find any free block after searching the entire disk
            tReturnCode = RETURN_ERROR_DISK_FULL;

        }

        // TODO: Validate the tree
        // ...
        
    }
    

    return tReturnCode;
}


BasicReturnCode HMFS_NVDM_save( struct NVDataManager *pNVDataManager,
                                void *pNVData,
                                int pNVDataLength) {

    uint32_t tNVBlockAddress = (HMFS_DISK_SIZE / HMFS_BLOCK_SIZE);
    DoubleWord tCRC;

    tCRC.dword = CRC_calculateCRC32(pNVData, pNVDataLength);
    
    memcpy(gHMFS_blockDataBuffer, pNVData, pNVDataLength);
    memcpy(gHMFS_blockDataBuffer + pNVDataLength, tCRC.byte, 4);
    
    //All NVdata is saved one block after HMFS area, plus index
    tNVBlockAddress += pNVDataManager->index;
    
    BasicReturnCode tResult = HMFS_DIO_writeBlock(tNVBlockAddress, gHMFS_blockDataBuffer);
    
    //Read and check data?
    
    return tResult;
}


BasicReturnCode HMFS_NVDM_load( struct NVDataManager *pNVDataManager,
                                void *pNVData,
                                int pNVDataLength) {

    uint32_t tNVBlockAddress = (HMFS_DISK_SIZE / HMFS_BLOCK_SIZE);
    DoubleWord tCalculatedCRC, tReadCRC;
    
    BasicReturnCode tReturnCode = RETURN_ERROR_READ_FAILED;
    BasicReturnCode tResult;

    tNVBlockAddress += pNVDataManager->index;
    
    tResult = HMFS_DIO_readBlock(tNVBlockAddress, gHMFS_blockDataBuffer);

    if(tResult == RETURN_SUCCESS) {

        tCalculatedCRC.dword = CRC_calculateCRC32(gHMFS_blockDataBuffer, pNVDataLength);
        
        memcpy(tReadCRC.byte, gHMFS_blockDataBuffer + pNVDataLength, 4);
        
        if(tCalculatedCRC.dword == tReadCRC.dword) {
            
            memcpy(pNVData, gHMFS_blockDataBuffer, pNVDataLength);
            
            tReturnCode = RETURN_SUCCESS;
            
        } else {
            
            tReturnCode = RETURN_ERROR_INVALID_CRC;
            
        }
        
    }
    
    return tReturnCode;
}


BasicReturnCode HMFS_initializeNVDataManager(   struct NVDataManager *pNVDataManager,
                                                uint8_t pIndex) {
    
    BasicReturnCode tReturnCode = RETURN_SUCCESS;
    
    pNVDataManager->index = pIndex;
    pNVDataManager->load = HMFS_NVDM_load;
    pNVDataManager->save = HMFS_NVDM_save;
    
    return tReturnCode;
}


BasicReturnCode HMFS_newBlock(uint32_t* tFreeBlockAddress) {

    BasicReturnCode tReturnCode = RETURN_ERROR;
    BasicReturnCode tResult;
        
    uint32_t tPreviousFirst;
    uint32_t tCalculatedCRC;

    if (gHMFS_firstFreeBlockAddress != 0) {
        
        tReturnCode = RETURN_SUCCESS;

        (*tFreeBlockAddress) = gHMFS_firstFreeBlockAddress;

        struct FreeDataBlock *tFreeBlock = (struct FreeDataBlock *)(gHMFS_newBlock_blockDataBuffer);

        tReturnCode = HMFS_DIO_readBlock(gHMFS_firstFreeBlockAddress, gHMFS_newBlock_blockDataBuffer);
        
        if (tReturnCode == RETURN_SUCCESS) {

            tReturnCode = HMFS_checkFreeBlock(tFreeBlock);

            if (tReturnCode == RETURN_SUCCESS) {

                tPreviousFirst = gHMFS_firstFreeBlockAddress;
                gHMFS_firstFreeBlockAddress = tFreeBlock->next;
                gHMFS_numFreeBlocks--;
                
                //Update the next free block in the chain
                tReturnCode = HMFS_DIO_readBlock(gHMFS_firstFreeBlockAddress, gHMFS_newBlock_blockDataBuffer);
                
                tFreeBlock->prev = 0;
                tFreeBlock->crc = 0;
                
                tCalculatedCRC = CRC_calculateCRC32((uint8_t*)(tFreeBlock), sizeof(struct FreeDataBlock));
                tFreeBlock->crc = tCalculatedCRC;

                tResult = HMFS_DIO_writeBlock(gHMFS_firstFreeBlockAddress, gHMFS_newBlock_blockDataBuffer);
                
                if(tResult != RETURN_SUCCESS) {
                    gHMFS_firstFreeBlockAddress = tPreviousFirst;
                    tReturnCode = RETURN_UNDEFINED_ERROR;
                }
                

            }

        }

        if (tReturnCode != RETURN_SUCCESS) {

            if (gHMFS_lastFreeBlockAddress != 0) {

                gHMFS_firstFreeBlockAddress = gHMFS_lastFreeBlockAddress;

            } else {

                gHMFS_firstFreeBlockAddress = 0;

            }

            tReturnCode = RETURN_SUCCESS;

        }
        
        //Update pointers to NVData
        if(tReturnCode == RETURN_SUCCESS
        && gHMFS_pointersNVDataManager != 0) {
            
            gHMFS_numFreeBlocks = 0;
    
            struct HMFSPointerNVData tNVData;
            
            tNVData.firstFreeBlock = gHMFS_firstFreeBlockAddress;
            tNVData.lastFreeBlock = gHMFS_lastFreeBlockAddress;
            tNVData.numFreeBlocks = gHMFS_numFreeBlocks;
    
            tReturnCode = gHMFS_pointersNVDataManager->save(gHMFS_pointersNVDataManager,
                                                            &tNVData,
                                                            sizeof(struct HMFSPointerNVData));
            
        }

    }

    return tReturnCode;

}


BasicReturnCode HMFS_checkFreeBlock(struct FreeDataBlock *pFreeBlock) {

    BasicReturnCode tReturnCode = RETURN_ERROR;
    
    uint32_t tReadCRC;
    uint32_t tCalculatedCRC;
    
    if (pFreeBlock->magic == HMFS_FREE_BLOCK_MAGIC) {
            
        tReadCRC = pFreeBlock->crc;

        pFreeBlock->crc = 0;
        tCalculatedCRC = CRC_calculateCRC32((uint8_t *)(pFreeBlock), sizeof(struct FreeDataBlock));
        
        pFreeBlock->crc = tReadCRC;
        
        if (tReadCRC == tCalculatedCRC) {
            tReturnCode = RETURN_SUCCESS;
        } else {
            tReturnCode = RETURN_ERROR_INVALID_CRC;
        }
        
    }
    
    return tReturnCode;
}


BasicReturnCode HMFS_checkDataBlock(struct ResourceDataBlock *pDataBlock) {

    BasicReturnCode tReturnCode = RETURN_ERROR;
    uint32_t tCalculatedCRC;
    uint32_t tReadCRC;
    
    if (pDataBlock->magic == HMFS_DATA_BLOCK_MAGIC) {
        
        tReadCRC = pDataBlock->crc;
        
        pDataBlock->crc = 0;
        tCalculatedCRC = CRC_calculateCRC32((uint8_t*)(pDataBlock), sizeof(struct ResourceDataBlock));
        
        pDataBlock->crc = tReadCRC;
        
        if (tReadCRC == tCalculatedCRC) {
            tReturnCode = RETURN_SUCCESS;
        } else {
            tReturnCode = RETURN_ERROR_INVALID_CRC;
        }

    }
    
    return tReturnCode;

}


BasicReturnCode HMFS_checkNodeBlock(struct ResourceNodeBlock *pNodeBlock) {

    BasicReturnCode tReturnCode = RETURN_ERROR;
    uint32_t tCalculatedCRC;
    uint32_t tReadCRC;
    
    if (pNodeBlock->magic == HMFS_NODE_BLOCK_MAGIC
    && pNodeBlock->numKeys < HMFS_MAX_BTREE_DEGREE) {
    
        tReadCRC = pNodeBlock->crc;
        
        pNodeBlock->crc = 0;
        tCalculatedCRC = CRC_calculateCRC32((uint8_t*)(pNodeBlock), sizeof(struct ResourceNodeBlock));
        
        pNodeBlock->crc = tReadCRC;
        
        if (tReadCRC == tCalculatedCRC) {
            tReturnCode = RETURN_SUCCESS;
        } else {
            tReturnCode = RETURN_ERROR_INVALID_CRC;
        }

    }
    
    return tReturnCode;

}


uint8_t HMFS_hashEquals(uint8_t *pHashA, uint8_t *pHashB) {
	
	int i;
	
	for(i = 0; i < HMFS_HASH_LENGTH; i++) {
		
		if (pHashA[i] != pHashB[i]) {
			return 0;
		}
		
		//Continue only if A[i] == B[i]
		
	}
	
	return 1;

}

uint8_t HMFS_hashIsLesser(uint8_t *pHashA, uint8_t *pHashB) {

	int i;
	
	for(i = 0; i < HMFS_HASH_LENGTH; i++) {
		
		if (pHashA[i] < pHashB[i]) {

			return 1;

		} else if (pHashA[i] > pHashB[i]) {

    		return 0;

		}
		
		//Continue only if A[i] == B[i]
		
	}
	
	return 0;
    
}

uint8_t HMFS_hashIsGreater(uint8_t *pHashA, uint8_t *pHashB) {

	int i;
	
	for(i = 0; i < HMFS_HASH_LENGTH; i++) {
		
		if (pHashA[i] > pHashB[i]) {

			return 1;
            
		} else if (pHashA[i] < pHashB[i]) {

			return 0;
		}
		
		//Continue only if A[i] == B[i]
		
	}
	
	return 0;
    
}


BasicReturnCode HMFS_searchKeyInNode(   struct ResourceNodeBlock *pNodeBlock,
                                        uint8_t *pHash,
                                        uint32_t *pKeyBlockAddress) {

    BasicReturnCode tReturnCode = RETURN_ERROR;

    uint8_t i;
    
    for (i = 0; i < pNodeBlock->numKeys; i++) {
                    
        if (HMFS_hashEquals(pHash, pNodeBlock->resourceHash[i])) {

            (*pKeyBlockAddress) = pNodeBlock->resourceBlockAddress[i];

            tReturnCode = RETURN_SUCCESS;
            
            break;

        }
        
    }
 
    return tReturnCode;
       
}


BasicReturnCode HMFS_searchChildInNode( struct ResourceNodeBlock *pNodeBlock,
                                        uint8_t *pHash,
                                        uint32_t *pChildBlockAddress) {

    BasicReturnCode tReturnCode = RETURN_ERROR;

    uint8_t i;

    if (pNodeBlock->numKeys == 0) {

        (*pChildBlockAddress) = 0;

        tReturnCode = RETURN_SUCCESS;

    } else {

        for (i = 0; i < pNodeBlock->numKeys; i++) {
            
            if (HMFS_hashIsLesser(pHash, pNodeBlock->resourceHash[i])) {

                (*pChildBlockAddress) = pNodeBlock->childNodeAddress[i];

                tReturnCode = RETURN_SUCCESS;

                break;

            }

            if (i == (pNodeBlock->numKeys-1)) {

                if (HMFS_hashIsGreater(pHash, pNodeBlock->resourceHash[i])) {
            
                    (*pChildBlockAddress) = pNodeBlock->childNodeAddress[i+1];

                    tReturnCode = RETURN_SUCCESS;
                    
                }

            }
            
        }

    }

    return tReturnCode;

}


BasicReturnCode HMFS_updateNodeBlockCRC(struct ResourceNodeBlock *pNodeBlock) {
    
    DoubleWord tCRC;
    
    pNodeBlock->crc = 0;
    
    tCRC.dword = CRC_calculateCRC32((uint8_t *)pNodeBlock, sizeof(struct ResourceNodeBlock));
    
    pNodeBlock->crc = tCRC.dword;
    
    return RETURN_SUCCESS;
}


BasicReturnCode HMFS_insertKeyInNode(   struct ResourceNodeBlock *pNodeBlock,
                                        uint32_t pNodeAddress,
                                        uint8_t *pKeyHash,
                                        uint32_t pKeyAddress,
                                        uint32_t pRightChild) {

    BasicReturnCode tReturnCode = RETURN_SUCCESS;
    
    uint8_t i;
    uint8_t tIndex = 0;
    
    //Find the keys to insert between
    for (i = 0; i < pNodeBlock->numKeys; i++) {
        
        if (HMFS_hashIsGreater(pKeyHash, pNodeBlock->resourceHash[i])) {
            tIndex = i+1;
        } else {
            break;
        }

    }
    
    // Open a gap in the desired index
    for (i = pNodeBlock->numKeys; i > tIndex; i--) {
    
        memcpy(pNodeBlock->resourceHash[i], pNodeBlock->resourceHash[i-1], HMFS_HASH_LENGTH);
        pNodeBlock->resourceBlockAddress[i] = pNodeBlock->resourceBlockAddress[i-1];
        
        if (i == pNodeBlock->numKeys) {
            pNodeBlock->childNodeAddress[i+1] = pNodeBlock->childNodeAddress[i];
        }
        
        pNodeBlock->childNodeAddress[i] = pNodeBlock->childNodeAddress[i-1];
        
    }
    
    // Fill the gap with key and right child node address
    memcpy(pNodeBlock->resourceHash[tIndex], pKeyHash, HMFS_HASH_LENGTH);
    pNodeBlock->resourceBlockAddress[tIndex] = pKeyAddress;
    pNodeBlock->childNodeAddress[tIndex+1] = pRightChild;
    pNodeBlock->numKeys++;

    HMFS_updateNodeBlockCRC(pNodeBlock);
    
    HMFS_DIO_writeBlock(pNodeAddress, ((uint8_t*)pNodeBlock));
    
    return tReturnCode;

}


BasicReturnCode HMFS_splitRoot( struct ResourceNodeBlock *pNodeBlock) {

    BasicReturnCode tReturnCode = RETURN_ERROR;
    
    BasicReturnCode tResult;
    uint8_t i;
    uint32_t tNewLeftBlockAddress;
    uint32_t tNewRightBlockAddress;
        
    if (pNodeBlock->numKeys > 2) {
    
        //Get the median value
        uint16_t tMedianIndex = pNodeBlock->numKeys / 2;

        ///// LEFT BLOCK /////

        //Create a new node
        struct ResourceNodeBlock *tNewLeftBlock = (struct ResourceNodeBlock *) gHMFS_splitBlock_blockDataBuffer;
        memset(gHMFS_splitBlock_blockDataBuffer, 0x00, HMFS_BLOCK_SIZE); // todo - factory method

        //Create new left block
        tResult = HMFS_newBlock(&tNewLeftBlockAddress);

        if (tResult == RETURN_SUCCESS) {

            tNewLeftBlock->magic = HMFS_NODE_BLOCK_MAGIC;

            tNewLeftBlock->numKeys = tMedianIndex;

            //copy values at the left of the median index to new node
            for (i = 0; i < tMedianIndex; i++) {

                memcpy(tNewLeftBlock->resourceHash[i], pNodeBlock->resourceHash[i], HMFS_HASH_LENGTH);
                tNewLeftBlock->resourceBlockAddress[i] = pNodeBlock->resourceBlockAddress[i];

                tNewLeftBlock->childNodeAddress[i] = pNodeBlock->childNodeAddress[i];

                if (i+1 == tMedianIndex) {
                    tNewLeftBlock->childNodeAddress[i+1] = pNodeBlock->childNodeAddress[i+1];
                }

            }

            HMFS_updateNodeBlockCRC(tNewLeftBlock);

            //Write changes to disk
            HMFS_DIO_writeBlock(tNewLeftBlockAddress, gHMFS_splitBlock_blockDataBuffer);

        } else {

            tReturnCode = RETURN_ERROR_INVALID_PARAMETERS;

            return tReturnCode;

        }

        ///// RIGHT BLOCK /////

        //Create a new node
        struct ResourceNodeBlock *tNewRightBlock = (struct ResourceNodeBlock *) gHMFS_splitBlock_blockDataBuffer;
        memset(gHMFS_splitBlock_blockDataBuffer, 0x00, HMFS_BLOCK_SIZE);

        //Create new right block
        tResult = HMFS_newBlock(&tNewRightBlockAddress);

        if (tResult == RETURN_SUCCESS) {

            tNewRightBlock->magic = HMFS_NODE_BLOCK_MAGIC;

            tNewRightBlock->numKeys = (pNodeBlock->numKeys - tMedianIndex - 1);

            //copy values at the right of the median index to new node
            for (i = tMedianIndex+1; i < pNodeBlock->numKeys; i++) {

                memcpy(tNewRightBlock->resourceHash[(i-(tMedianIndex+1))], pNodeBlock->resourceHash[i], HMFS_HASH_LENGTH);
                tNewRightBlock->resourceBlockAddress[(i-(tMedianIndex+1))] = pNodeBlock->resourceBlockAddress[i];

                tNewRightBlock->childNodeAddress[(i-(tMedianIndex+1))] = pNodeBlock->childNodeAddress[i];

                if (i+1 == pNodeBlock->numKeys) {
                    tNewRightBlock->childNodeAddress[(i-(tMedianIndex+1))+1] = pNodeBlock->childNodeAddress[i+1];
                }

            }

            HMFS_updateNodeBlockCRC(tNewRightBlock);

            //Write changes to disk
            HMFS_DIO_writeBlock(tNewRightBlockAddress, gHMFS_splitBlock_blockDataBuffer);

        } else {
        
            tReturnCode = RETURN_ERROR_INVALID_PARAMETERS;

            return tReturnCode;

        }

        ///// FIX ROOT BLOCK /////
        pNodeBlock->numKeys = 1;

        //Move median value to index 0
        memcpy(pNodeBlock->resourceHash[0], pNodeBlock->resourceHash[tMedianIndex], HMFS_HASH_LENGTH);
        pNodeBlock->resourceBlockAddress[0] = pNodeBlock->resourceBlockAddress[tMedianIndex];
        
        for (i = 1; i < HMFS_MAX_BTREE_DEGREE-1; i++) {
            memset(pNodeBlock->resourceHash[i], 0x00, HMFS_HASH_LENGTH);
            memset(&pNodeBlock->resourceBlockAddress[i], 0x00, 4);
            memset(&pNodeBlock->childNodeAddress[i], 0x00, 4);
        }

        memset(&pNodeBlock->childNodeAddress[HMFS_MAX_BTREE_DEGREE-1], 0x00, 4);

        //Link children
        pNodeBlock->childNodeAddress[0] = tNewLeftBlockAddress;
        pNodeBlock->childNodeAddress[1] = tNewRightBlockAddress;

        HMFS_updateNodeBlockCRC(pNodeBlock);

        //Write changes to disk
        HMFS_DIO_writeBlock(0, (uint8_t*)pNodeBlock);
        
    } else {
        
        tReturnCode = RETURN_ERROR_INVALID_PARAMETERS;

        return tReturnCode;
                
    }

    return tReturnCode;

}


BasicReturnCode HMFS_splitNode( uint32_t pParentNodeAddress,
                                uint32_t pChildNodeAddress,
                                struct ResourceNodeBlock *pChildNodeBlock,
                                uint8_t* pParentIsFull) {

    BasicReturnCode tReturnCode = RETURN_ERROR;
    
    BasicReturnCode tResult;
    uint8_t i;
    //uint32_t tNewLeftBlockAddress;
    uint32_t tNewRightBlockAddress;
        
    if (pChildNodeBlock->numKeys > 2) {
    
        //Get the median value
        uint16_t tMedianIndex = pChildNodeBlock->numKeys / 2;

        ///// RIGHT BLOCK /////

        //Create a new node
        struct ResourceNodeBlock *tNewRightBlock = (struct ResourceNodeBlock *) gHMFS_splitBlock_blockDataBuffer;
        memset(gHMFS_splitBlock_blockDataBuffer, 0x00, HMFS_BLOCK_SIZE); // todo - factory method

        //Create new right block
        tResult = HMFS_newBlock(&tNewRightBlockAddress);

        if (tResult == RETURN_SUCCESS) {

            tNewRightBlock->magic = HMFS_NODE_BLOCK_MAGIC;

            tNewRightBlock->numKeys = (pChildNodeBlock->numKeys - tMedianIndex - 1);
            
            //copy values at the right of the median index to new node
            for (i = tMedianIndex+1; i < pChildNodeBlock->numKeys; i++) {

                memcpy(tNewRightBlock->resourceHash[(i-(tMedianIndex+1))], pChildNodeBlock->resourceHash[i], HMFS_HASH_LENGTH);
                tNewRightBlock->resourceBlockAddress[(i-(tMedianIndex+1))] = pChildNodeBlock->resourceBlockAddress[i];

                tNewRightBlock->childNodeAddress[(i-(tMedianIndex+1))] = pChildNodeBlock->childNodeAddress[i];

                if (i+1 == pChildNodeBlock->numKeys) {
                    tNewRightBlock->childNodeAddress[(i-(tMedianIndex+1))+1] = pChildNodeBlock->childNodeAddress[i+1];
                }

            }

            HMFS_updateNodeBlockCRC(tNewRightBlock);

            //Write changes to disk
            HMFS_DIO_writeBlock(tNewRightBlockAddress, gHMFS_splitBlock_blockDataBuffer);

        } else {
        
            tReturnCode = RETURN_ERROR_INVALID_PARAMETERS;

            return tReturnCode;

        }

        ///// PARENT BLOCK /////

        struct ResourceNodeBlock *tParentNodeBlock = (struct ResourceNodeBlock *) gHMFS_splitBlock_blockDataBuffer;

        tResult = HMFS_DIO_readBlock(pParentNodeAddress, gHMFS_splitBlock_blockDataBuffer);

        if (tResult == RETURN_SUCCESS) {
            
            tResult = HMFS_insertKeyInNode( tParentNodeBlock,
                                            pParentNodeAddress,
                                            pChildNodeBlock->resourceHash[tMedianIndex],
                                            pChildNodeBlock->resourceBlockAddress[tMedianIndex],
                                            tNewRightBlockAddress);

            if (tResult == RETURN_SUCCESS) {
                
                (*pParentIsFull) = (tParentNodeBlock->numKeys == (HMFS_MAX_BTREE_DEGREE-1));

            }
            
        } else {

            tReturnCode = RETURN_ERROR_INVALID_PARAMETERS;

            return tReturnCode;

        }
        
        ///// CURRENT BLOCK (LEFT CHILD) /////

        pChildNodeBlock->numKeys = tMedianIndex;

        //Clean elements splited into another nodes
        for (i = tMedianIndex; i < HMFS_MAX_BTREE_DEGREE-1; i++) {
            memset(pChildNodeBlock->resourceHash[i], 0x00, HMFS_HASH_LENGTH);
            memset(&pChildNodeBlock->resourceBlockAddress[i], 0x00, 4);
            memset(&pChildNodeBlock->childNodeAddress[i+1], 0x00, 4);
        }

        HMFS_updateNodeBlockCRC(pChildNodeBlock);

        //Write changes to disk
        HMFS_DIO_writeBlock(pChildNodeAddress, (uint8_t*)pChildNodeBlock);

        return tReturnCode;
        
    }

    return tReturnCode;

}


BasicReturnCode HMFS_insertNodeBlock(   uint8_t *pHash,
                                        uint32_t pBlockAddress) {

    BasicReturnCode tReturnCode = RETURN_ERROR;
    BasicReturnCode tResult;
    
    uint32_t tNextBlockAddress;
    uint32_t tCurrentBlockAddress;
//    uint32_t tParentNodeAddress;
//    
//    uint8_t tSplitResourceKey[HMFS_HASH_LENGTH];
//    uint32_t tSplitResourceAddress;
//    uint32_t tSplitRightNodeAddress;
    
    uint16_t tStackIndex;
    uint32_t tNodeStack[HMFS_MAX_BTREE_DEPTH];
    
    struct ResourceNodeBlock *tCurrentNodeBlock = (struct ResourceNodeBlock *) gHMFS_blockDataBuffer;
    memset(gHMFS_blockDataBuffer, 0x00, HMFS_BLOCK_SIZE);
    
    tCurrentBlockAddress = 0;
    tStackIndex = 0;
    
    while (tStackIndex < HMFS_MAX_BTREE_DEPTH) {

        tResult = HMFS_DIO_readBlock(tCurrentBlockAddress, gHMFS_blockDataBuffer);

		if (tResult == RETURN_SUCCESS) {
			
			tResult = HMFS_checkNodeBlock(tCurrentNodeBlock);
			
			if (tResult == RETURN_SUCCESS) {

                uint32_t tKeyBlockAddress = 0;
                
                if (HMFS_searchKeyInNode(tCurrentNodeBlock, pHash, &tKeyBlockAddress) == RETURN_SUCCESS) {

                    //printHex(pHash, HMFS_HASH_LENGTH);
                    
                    //Key already registered
                    tReturnCode = RETURN_ERROR_DUP_KEY;

                    return tReturnCode;

                }
                
                tReturnCode = HMFS_searchChildInNode(tCurrentNodeBlock, pHash, &tNextBlockAddress);

                if (tReturnCode == RETURN_SUCCESS) {

                    if (tNextBlockAddress == 0) {
                        
                        // Insert in this block, leaf node has no child so pRightChild = 0
                        tResult = HMFS_insertKeyInNode(tCurrentNodeBlock, tCurrentBlockAddress, pHash, pBlockAddress, 0);
                        
                        if (tResult == RETURN_SUCCESS) {

                            //std::cout << "INSERTED KEY BLOCK" << std::endl;
                            //printHex(((uint8_t*)tCurrentNodeBlock), HMFS_BLOCK_SIZE);
                            
                            tNodeStack[tStackIndex] = tCurrentBlockAddress;
                            tStackIndex++;
                            
                            tReturnCode = RETURN_SUCCESS;

                            break;
                            
                        }
                        
                    } else {

                        tNodeStack[tStackIndex] = tCurrentBlockAddress;
                        tStackIndex++;
                        
                        //Continue to child
                        tCurrentBlockAddress = tNextBlockAddress;
                        
                    }

                } else {

                    //todo
                    break;

                }
                
            } else {

                if (tCurrentBlockAddress == 0) {

                    //create root node block

                    tCurrentNodeBlock->magic = HMFS_NODE_BLOCK_MAGIC;
                    tCurrentNodeBlock->numKeys = 1;
                    
                    memcpy(&tCurrentNodeBlock->resourceHash[0][0], pHash, HMFS_HASH_LENGTH);

                    tCurrentNodeBlock->resourceBlockAddress[0] = tCurrentBlockAddress;
                    
                    HMFS_updateNodeBlockCRC(tCurrentNodeBlock);

                    HMFS_DIO_writeBlock(tCurrentBlockAddress, gHMFS_blockDataBuffer);

                    break;
                    
                } else {
                    
                    //Corrupted block data
                    tReturnCode = RETURN_ERROR_CORRUTED_BLOCK;
                    break;

                }
                
            }
            
        } else {

            //Read block failed
            tReturnCode = RETURN_ERROR;
            
        }
        
    }

    if (tReturnCode == RETURN_SUCCESS) {

        int tCurrentStackIndex = (tStackIndex-2);

        // TODO RESULTS X RETURN CODE

        // Split full nodes up to the root, stop at first not full node
        while ((tReturnCode == RETURN_SUCCESS) && (tCurrentNodeBlock->numKeys == (HMFS_MAX_BTREE_DEGREE-1))) {

            if (tCurrentBlockAddress == 0) {

                tResult = HMFS_splitRoot(tCurrentNodeBlock);
                
            } else {

                uint8_t tParentIsFull = 0;
                
                tResult = HMFS_splitNode(tNodeStack[tCurrentStackIndex], tCurrentBlockAddress, tCurrentNodeBlock, &tParentIsFull);

                if (tReturnCode == RETURN_SUCCESS) {

                    if (tParentIsFull != 0) {

                        tCurrentBlockAddress = tNodeStack[tCurrentStackIndex];

                        tResult = HMFS_DIO_readBlock(tCurrentBlockAddress, gHMFS_blockDataBuffer);

                        if (tReturnCode == RETURN_SUCCESS) {

                            tCurrentStackIndex -= 1;

                        }

                    }

                }
                
            }
            
        }
        
    }

    return tReturnCode;
}


BasicReturnCode HMFS_getResource(uint8_t *pHash, uint8_t *pData, uint32_t *pDataSize) {

    BasicReturnCode tReturnCode = RETURN_ERROR;
    BasicReturnCode tResult;

    struct ResourceDataBlock *tCurrentDataBlock = (struct ResourceDataBlock *) gHMFS_blockDataBuffer;

    uint8_t *tReadPointer;
    uint32_t tDataSize;
    uint32_t tDataBlockAddress;
    uint32_t tNextDataBlock;

    tDataBlockAddress = HMFS_searchResourceAddress(pHash);

    if (tDataBlockAddress != 0) {

        tNextDataBlock = tDataBlockAddress;
        
        tReadPointer = pData;
        tDataSize = 0;
        
        while (tNextDataBlock != 0) {
            
            tResult = HMFS_DIO_readBlock(tNextDataBlock, gHMFS_blockDataBuffer);

            if (tResult == RETURN_SUCCESS) {

                tResult = HMFS_checkDataBlock(tCurrentDataBlock);

                if (tResult == RETURN_SUCCESS) {
                    
                    memcpy(tReadPointer, tCurrentDataBlock->data, tCurrentDataBlock->dataSize);
                    tReadPointer += tCurrentDataBlock->dataSize;
                    tDataSize += tCurrentDataBlock->dataSize;
                            
                    tNextDataBlock = tCurrentDataBlock->nextDataBlock;

                    if (tNextDataBlock == 0) {
                        tReturnCode = RETURN_SUCCESS;
                    }
                    
                } else {

                    tReturnCode = RETURN_ERROR;

                    break;
                    
                }

            } else {

                tReturnCode = RETURN_ERROR_READ_FAILED;

                break;

            }
            
        }
        
        if (pDataSize != 0) {
            *pDataSize = tDataSize;
        }
        
    } else {
        
        //Key not found
        tReturnCode = RETURN_ERROR_NOT_FOUND;

    }

    return tReturnCode;

}


uint32_t HMFS_searchResourceAddress(uint8_t *pHash) {
    
    uint32_t tCurrentNodeAddress;
    uint32_t tResourceAddress;
    
    BasicReturnCode tResult;
    
    struct ResourceNodeBlock *tCurrentNodeBlock = (struct ResourceNodeBlock *) gHMFS_blockDataBuffer;
    
    tCurrentNodeAddress = 0;
    tResourceAddress = 0;
    
    do {
        
        tResult = HMFS_DIO_readBlock(tCurrentNodeAddress, gHMFS_blockDataBuffer);
        
        if (tResult == RETURN_SUCCESS) {
            
            tResult = HMFS_checkNodeBlock(tCurrentNodeBlock);
            
            if (tResult == RETURN_SUCCESS) {
                
                HMFS_searchKeyInNode(tCurrentNodeBlock, pHash, &tResourceAddress);
                
                if (tResourceAddress == 0) {
                    
                    //Continue looking in following children
                    HMFS_searchChildInNode(tCurrentNodeBlock, pHash, &tCurrentNodeAddress);
                    
                } else {
                    
                    //Resource found in current block
                    break;
                }
                
            } else {

                break;

            }
            
        } else {

            break;

        }
        
    } while(tCurrentNodeAddress != 0);
    
    return tResourceAddress;

}


BasicReturnCode HMFS_storeResource(uint8_t *pData, uint32_t pDataSize, uint8_t *pHash) {

    BasicReturnCode tReturnCode = RETURN_ERROR;
    
    SHA256Context tSHA256Context;
    uint32_t tCurrentBlockAddress;
    uint32_t tNextBlockAddress;
    uint32_t tFirstDataBlockAddress;
    uint32_t tBytesRemaining;
    uint32_t tCalculatedCRC;
    uint32_t tSearchBlockAddress;
    uint8_t *tDataPointer;
    
	if (pHash != 0) {
        
        sha256_init(&tSHA256Context);
        sha256_update(&tSHA256Context, pData, pDataSize);
        sha256_final(&tSHA256Context, pHash);

        //Before adding resource, must verify if it does not exist already
        tSearchBlockAddress = HMFS_searchResourceAddress(pHash);
        
        if(tSearchBlockAddress == 0) {
        
            //Resource was not previously stored, proceed with new resource creation
            
            tBytesRemaining = pDataSize;
            tDataPointer = pData;

            BasicReturnCode tResult = HMFS_newBlock(&tFirstDataBlockAddress);

            if (tResult == RETURN_SUCCESS) {

                tNextBlockAddress = tFirstDataBlockAddress;

                while (tNextBlockAddress != 0) {

                    tCurrentBlockAddress = tNextBlockAddress;

                    struct ResourceDataBlock *tCurrentResourceDataBlock = (struct ResourceDataBlock *) gHMFS_blockDataBuffer;
                    memset(&gHMFS_blockDataBuffer, 0x00, sizeof (struct ResourceDataBlock));

                    tCurrentResourceDataBlock->magic = HMFS_DATA_BLOCK_MAGIC;

                    if(tBytesRemaining < HMFS_MAX_DATA_PER_BLOCK) {
                        tCurrentResourceDataBlock->dataSize = tBytesRemaining;
                    } else {
                        tCurrentResourceDataBlock->dataSize = HMFS_MAX_DATA_PER_BLOCK;
                    }

                    tBytesRemaining -= tCurrentResourceDataBlock->dataSize;

                    if (tBytesRemaining > 0) {

                        BasicReturnCode tResult = HMFS_newBlock(&tNextBlockAddress);

                        if (tResult == RETURN_ERROR) {
                            tReturnCode = RETURN_ERROR_DISK_FULL;
                            break;
                        }

                    } else {

                        tNextBlockAddress = 0;

                    }

                    tCurrentResourceDataBlock->nextDataBlock = tNextBlockAddress;

                    memcpy(tCurrentResourceDataBlock->data, tDataPointer, tCurrentResourceDataBlock->dataSize);

                    tCurrentResourceDataBlock->crc = 0;

                    tCalculatedCRC = CRC_calculateCRC32((uint8_t*)(tCurrentResourceDataBlock), sizeof(struct ResourceDataBlock));
                    tCurrentResourceDataBlock->crc = tCalculatedCRC;

                    HMFS_DIO_writeBlock(tCurrentBlockAddress, gHMFS_blockDataBuffer);

                    tDataPointer += tCurrentResourceDataBlock->dataSize;

                }

                tReturnCode = HMFS_insertNodeBlock(pHash, tFirstDataBlockAddress);

            } else {

                //Failed to allocate the first data block
                tReturnCode = RETURN_ERROR_DISK_FULL;

            }

        } else {

            //Resoucre already exists, consider success
            tReturnCode = RETURN_SUCCESS;

        }
        
    }

	return tReturnCode;

}
