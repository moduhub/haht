
#include "file/hmfs_diskio.h"

#include "peripheral/SPI.h"
#include "peripheral/PIC32SPI.h"

#include "sd/SDcard.h"


struct SPIInstance  gSDCardSPI;
struct SDCard       gSDCard;

//uint8_t gBlockBuffer[512];

uint8_t gSDResult;

BasicReturnCode HMFS_DIO_initialize() {

    BasicReturnCode tReturnCode = RETURN_SUCCESS;
    
    SD_configureSPI(&gSDCard, &gSDCardSPI);
    PIC32_SPI2_configure(&gSDCardSPI);
    
    uint8_t tAttempts = 0;
    
    do {
        
        SYS_delay(1000);
        
        if(++tAttempts > 10) {
            tReturnCode = RETURN_ERROR;
            break;
        }
        
        gSDResult = SD_initialize(&gSDCard);
        
    } while(gSDResult != SDC_SUCCESS);

	return tReturnCode;
}


BasicReturnCode HMFS_DIO_readBlock(uint32_t pBlockAddress, uint8_t *pData) {

    uint8_t tRetry = 0;
    while(SD_readBlock(&gSDCard, pBlockAddress * 512, pData) != SDC_SUCCESS) {

        if(++tRetry > 3) {
            return RETURN_ERROR;
        }

    }
    
	return RETURN_SUCCESS;
}

BasicReturnCode HMFS_DIO_checkBlock(uint32_t pBlockAddress, uint8_t *pData) {

    uint8_t tRetry = 0;
    while(SD_checkBlock(&gSDCard, pBlockAddress * 512, pData) != SDC_SUCCESS) {

        if(++tRetry > 3) {
            return RETURN_ERROR;
        }

    }
    
	return RETURN_SUCCESS;
}


BasicReturnCode HMFS_DIO_writeBlock(uint32_t pBlockAddress, uint8_t *pData) {

    uint8_t tResult;
    uint8_t tRetry = 0;
        
    while(++tRetry <= 3) {

        tResult = SD_writeBlock(&gSDCard, pBlockAddress * 512, pData);

        if(tResult == SDC_SUCCESS) {
            break;
        }

    }

    if(tResult != SDC_SUCCESS) {
        return RETURN_ERROR;
    }
    
	return RETURN_SUCCESS;
}
