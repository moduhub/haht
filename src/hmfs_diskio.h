
#ifndef _DISKIO
#define _DISKIO

#include "utils/BasicTypes.h"

BasicReturnCode HMFS_DIO_initialize();

BasicReturnCode HMFS_DIO_readBlock(uint32_t pBlockAddress, uint8_t *pData);

BasicReturnCode HMFS_DIO_writeBlock(uint32_t pBlockAddress, uint8_t *pData);

BasicReturnCode HMFS_DIO_checkBlock(uint32_t pBlockAddress, uint8_t *pData);


#endif
